Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: wxformbuilder
Upstream-Contact: Steffen Olszewski <steffen.olszewski@gero-mess.de>
Source: https://github.com/wxFormBuilder/wxFormBuilder
License: GPL-2+

Files: *
Copyright: 2005 José Antonio Hurtado <joseantonio.hurtado@gmail.com>
           2005 Juan Antonio Ortega
           2006-2007 Ryan Mulder <rjmyst3@gmail.com>
           2006 Ryan Pusztai <rpusztai@gmail.com>
           2007 Michal Bliznak
           2003  Yiannis An. Mandravellos
           2016 Jan Niklas Hasse
           2018-2021 Steffen Olszewski
License: GPL-2+

Files: debian/*
Copyright: 2023-2024 Steve Meliza <swm@swm1.com>
           2021 Steffen Olszewski
License: GPL-2+

Files: data/platform/linux/share/metainfo/*
Copyright: 2007 Ryan Mulder <rjmyst3@gmail.com>
           2021 Steffen Olszewski
License: CC0-1.0

Files: plugins/additional/additional.cpp
Copyright: 2005 José Antonio Hurtado
           2005 Juan Antonio Ortega
           1998-2005 Julian Smart, Robert Roebling et al
License: GPL-2+

Files: third_party/md5/*
Copyright: 1995 Mordechai T. Abzug
           1991-1992, RSA Data Security, Inc
License: RSA-MD

Files: third_party/stack_trace/*
Copyright: 2007 Edd Dawson
License: BSL-1.0

Files: third_party/tinyxml2/*
Copyright: 2000-2006 Lee Thomason
           2012 Matt Janisz
           2005 Tyge Lovset
           Yves Berquin
           2020 Dylan Baker
License: ZLIB

Files: third_party/tinyxml2/docs/*
Copyright: 1997-2023 by Dimitri van Heesch
           2021-2022 jothepro
License: MIT

Files: third_party/tinyxml2/docs/jquery.js
Copyright: jQuery Foundation and other contributors
           2007 Ariel Flesler
           2018 Steven Benner
           2011–2014 Dave Furfero
           Vasil Dinkov
License: MIT or GPL-2

Files: third_party/tinyxml2/resources/dream.xml
Copyright: Public Domain
License: public-domain
         Due to being a 16th century Shakespeare play.

License: GPL-2
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 '/usr/share/common-licenses/GPL-2'.

License: GPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 '/usr/share/common-licenses/GPL-2'.

License: CC0-1.0
  The person who associated a work with this deed has dedicated the work
  to the public domain by waiving all of his or her rights to the work
  worldwide under copyright law, including all related and neighboring rights,
  to the extent allowed by law.
  .
  You can copy, modify, distribute and perform the work, even for commercial
  purposes, all without asking permission.

License: RSA-MD
 C++/object oriented translation and modification of MD5.
 .
 Version: 1.00 (28 Aug 95)
 Version: 1.02 (22 Sep 97)
 .
  Translation and modification (c) 1995 by Mordechai T. Abzug 
  Thanks to Martin Cleaver for for making it happy on Windows NT and Solaris.
 .
  This translation/ modification is provided "as is," without express or 
  implied warranty of any kind.
 .
  The translator/ modifier does not claim (1) that MD5 will do what you think 
  it does; (2) that this translation/ modification is accurate; or (3) that 
  this software is "merchantible."  (Language for this disclaimer partially 
  copied from the disclaimer below).
 .
 Based on:
 .
    MD5.H - header file for MD5C.C
    MD5C.C - RSA Data Security, Inc., MD5 message-digest algorithm
    MDDRIVER.C - test driver for MD2, MD4 and MD5
 .
    Copyright (C) 1991-2, RSA Data Security, Inc. Created 1991. All
 rights reserved.
 .
 License to copy and use this software is granted provided that it
 is identified as the "RSA Data Security, Inc. MD5 Message-Digest
 Algorithm" in all material mentioning or referencing this software
 or this function.
 .
 License is also granted to make and use derivative works provided
 that such works are identified as "derived from the RSA Data
 Security, Inc. MD5 Message-Digest Algorithm" in all material
 mentioning or referencing the derived work.
 .
 RSA Data Security, Inc. makes no representations concerning either
 the merchantability of this software or the suitability of this
 software for any particular purpose. It is provided "as is"
 without express or implied warranty of any kind.
 .
 These notices must be retained in any copies of any part of this
 documentation and/or software.

License: BSL-1.0
 Boost Software License - Version 1.0 - August 17th, 2003
 .
 Permission is hereby granted, free of charge, to any person or organization
 obtaining a copy of the software and accompanying documentation covered by
 this license (the "Software") to use, reproduce, display, distribute,
 execute, and transmit the Software, and to prepare derivative works of the
 Software, and to permit third-parties to whom the Software is furnished to
 do so, all subject to the following:
 .
 The copyright notices in the Software and this entire statement, including
 the above license grant, this restriction and the following disclaimer,
 must be included in all copies of the Software, in whole or in part, and
 all derivative works of the Software, unless such copies or derivative
 works are solely in the form of machine-executable object code generated by
 a source language processor.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

License: ZLIB
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any
 damages arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any
 purpose, including commercial applications, and to alter it and
 redistribute it freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must
 not claim that you wrote the original software. If you use this
 software in a product, an acknowledgment in the product documentation
 would be appreciated but is not required.
 .
 2. Altered source versions must be plainly marked as such, and
 must not be misrepresented as being the original software.
 .
 3. This notice may not be removed or altered from any source
 distribution.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
